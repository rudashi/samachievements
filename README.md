Sam Achievements
================

Simple, elegant Achievements for SAM applications.

![Totem.com.pl](https://www.totem.com.pl/wp-content/uploads/2016/06/logo.png)

## General System Requirements

- [PHP >7.4.0](http://php.net/)
- [Laravel ~6.*](https://github.com/laravel/framework)
- [SAM-core ~1.*](https://bitbucket.org/rudashi/samcore)
- [SAM-Users ~1.*](https://bitbucket.org/rudashi/samusers)

## Quick Installation

If necessary, use the composer to download the library

```
$ composer require totem-it/sam-achievements
```

Remember to put repository in a composer.json

```
"repositories": [
    {
        "type": "vcs",
        "url":  "https://bitbucket.org/rudashi/samachievements.git"
    }
],
```

## Usage

To register an Achievement use command:

```bash
$ php artisan register:achievement {name : Class name of an achievement}
```

### API

Check [openapi](openapi.json) file.

## Authors

* **Borys Żmuda** - Lead designer - [LinkedIn](https://www.linkedin.com/in/boryszmuda/), [Portfolio](https://rudashi.github.io/)
