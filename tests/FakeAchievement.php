<?php

namespace Tests;

use Totem\SamAchievements\App\Model\Achievement;
use Totem\SamAchievements\App\Model\Contracts\Achievable;

class FakeAchievement extends Achievement implements Achievable
{

    public function setName(): string
    {
        return 'Fake Achievement';
    }

    public function setDescription(): string
    {
       return 'Fake Description';
    }

    public function setIcon(): ?string
    {
        return null;
    }

}
