<?php

namespace Tests;

use Totem\SamAchievements\App\Model\Achievement;
use Totem\SamAchievements\App\Repositories\AchievementRepository;
use Totem\SamAchievements\App\Traits\AchieverInterface;
use Totem\SamAcl\Testing\AssertForbiddenCall;
use Totem\SamAcl\Testing\AttachRoleToUserTrait;
use Totem\SamAdmin\Testing\ApiTest;

class AchievementsApiTest extends ApiTest
{
    use AttachRoleToUserTrait,
        AssertForbiddenCall;

    protected string $endpoint = 'achievements';
    protected string $model = FakeAchievement::class;

    public function test_get_all_trophies(): void
    {
        $model = $this->createModel();
        $assert_model[class_basename($model->type)] = $model->id;

        $this->get('/api/trophy')
            ->assertOk()
            ->assertJsonStructure([
                'data' => [],
                'apiVersion'
            ])
            ->assertJsonFragment($assert_model);
    }

    public function test_get_all(): void
    {
        $model = $this->createModel();

        $this->get("/api/$this->endpoint")
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'name',
                        'icon',
                        'description',
                        'points',
                    ]
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'id' => $model->id,
                'name' => $model->name,
                'icon' => $model->icon,
                'description' => $model->description,
                'points' => $model->points,
            ]);
    }

    public function test_unlock_achievement(): void
    {
        /** @var AchieverInterface $user */
        $user = $this->setUser();
        $model = $this->createModel(['points' => 10]);

        $this->post("/api/$this->endpoint/$model->id/unlock/$user->id", ['uuid' => $model->id])
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name',
                    'icon',
                    'description',
                    'progress',
                    'points',
                    'unlocked_at'
                ],
                'apiVersion'
            ])->assertJsonFragment([
                'id' => $model->id,
                'name' => $model->name,
                'icon' => $model->icon,
                'description' => $model->description,
                'progress' => 1,
                'points' => 10,
                'unlocked_at' => $user->achievements->first()->pivot->unlocked_at,
            ]);
    }

    public function test_unlock_second_progress_achievement(): void
    {
        /** @var AchieverInterface $user */
        $user = $this->setUser();
        $model = $this->createModel(['points' => 10]);

        $this->post("/api/$this->endpoint/$model->id/unlock/$user->id", ['uuid' => $model->id])
            ->assertOk()
            ->assertJsonFragment([
                'progress' => 1,
                'points' => 10,
            ]);

        $this->withToken()->post("/api/$this->endpoint/$model->id/unlock/$user->id", ['uuid' => $model->id])
            ->assertOk()
            ->assertJsonFragment([
                'progress' => 2,
                'points' => 10,
            ]);
    }

    public function test_reset_achievement(): void
    {
        /** @var AchieverInterface $user */
        $user = $this->setUser();
        $model = $this->createModel(['points' => 10]);

        $this->post("/api/$this->endpoint/$model->id/reset/$user->id", ['uuid' => $model->id])
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name',
                    'icon',
                    'description',
                    'progress',
                    'points',
                    'unlocked_at'
                ],
                'apiVersion'
            ])->assertJsonFragment([
                'id' => $model->id,
                'name' => $model->name,
                'icon' => $model->icon,
                'description' => $model->description,
                'progress' => 0,
                'points' => 10,
                'unlocked_at' => null,
            ]);
    }

    public function test_set_achievement(): void
    {
        /** @var AchieverInterface $user */
        $user = $this->setUser();
        $model = $this->createModel(['points' => 10]);

        $this->post("/api/$this->endpoint/$model->id/set/$user->id", ['uuid' => $model->id])
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name',
                    'icon',
                    'description',
                    'progress',
                    'points',
                    'unlocked_at'
                ],
                'apiVersion'
            ])->assertJsonFragment([
                'id' => $model->id,
                'name' => $model->name,
                'icon' => $model->icon,
                'description' => $model->description,
                'progress' => 10,
                'points' => 10,
                'unlocked_at' => $user->achievements->first()->pivot->unlocked_at,
            ]);
    }

    public function test_failed_action_validation(): void
    {
        $this->post("/api/$this->endpoint/d8bc6c8b-887f-4c3a-ae96-59a63935b9b0/unlock/0")
            ->assertStatus(422)
            ->assertJsonFragment([
                'error' => [
                    'code' => 422,
                    'message' => [
                        'uuid' => [ 'The uuid field is required.' ]
                    ]
                ],
            ]);
    }

    public function test_failed_action_incorrect_achievement_uuid(): void
    {
        $uuid = 'd8bc6c8b-887f-4c3a-ae96-59a63935b9b0';
        $uuid2 = '1c49e5a1-0643-4321-af64-bd6d68375a62';

        $this->post("/api/$this->endpoint/$uuid/unlock/0", ['uuid' => $uuid2])
            ->assertStatus(422)
            ->assertJsonFragment([
                'error' => [
                    'code' => 422,
                    'message' => __('Incorrect achievement parameters.')
                ],
            ]);
    }

    public function test_failed_action_incorrect_authenticated_user_id(): void
    {
        /** @var AchieverInterface $user */
        $uuid = 'd8bc6c8b-887f-4c3a-ae96-59a63935b9b0';

        $this->post("/api/$this->endpoint/$uuid/unlock/0", ['uuid' => $uuid])
            ->assertStatus(422)
            ->assertJsonFragment([
                'error' => [
                    'code' => 422,
                    'message' => __('Incorrect achievement parameters.')
                ],
            ]);
    }

    public function test_failed_action_not_exists_achievement(): void
    {
        /** @var AchieverInterface $user */
        $user = $this->setUser();
        $uuid = 'd8bc6c8b-887f-4c3a-ae96-59a63935b9b0';

        $this->post("/api/$this->endpoint/$uuid/unlock/$user->id", ['uuid' => $uuid])
            ->assertStatus(404)
            ->assertJsonFragment([
                'error' => [
                    'code' => 404,
                    'message' => __('Given id :code is invalid or achievement not exist.', ['code' => $uuid])
                ],
            ]);
    }

    public function test_get_user_achievements(): void
    {
        /** @var AchieverInterface $user */
        $user = $this->setUser();
        $model = $this->createModel();
        $repository = (new AchievementRepository())->setModel($this->model);
        $achievement = $repository->attachAchievement($user, $model);

        $this->get("/api/users/$user->id/$this->endpoint")
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'name',
                        'icon',
                        'description',
                        'progress',
                        'points',
                        'unlocked_at'
                    ]
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'id' => $model->id,
                'name' => $model->name,
                'icon' => $model->icon,
                'description' => $model->description,
                'progress' => $achievement->pivot->points,
                'points' => $model->points,
                'unlocked_at' => $achievement->pivot->unlocked_at->format('Y-m-d H:i:s'),
            ]);
    }

    public function test_get_user_empty_achievements(): void
    {
        $user = $this->setUser();
        $this->createModel();

        $this->get("/api/users/$user->id/$this->endpoint")
            ->assertOk()
            ->assertJsonStructure([
                'data',
                'apiVersion'
            ])
            ->assertJsonFragment([
                'data' => []
            ]);
    }

    private function createModel(array $attributes = []): Achievement
    {
        return factory($this->model)->create($attributes);
    }

}
