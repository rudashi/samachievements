<?php

namespace Totem\SamAchievements\App\Model;

use Totem\SamAchievements\App\Events\Progress;
use Totem\SamAchievements\App\Events\Unlocked;
use Totem\SamAchievements\App\Model\Contracts\AchievementInterface;
use Totem\SamCore\App\Traits\HasUuid;
use Totem\SamCore\App\Traits\EloquentDecoratorTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string name
 * @property string|null description
 * @property string|null icon
 * @property int points
 * @property bool secret
 * @property string type
 */
class Achievement extends Model implements AchievementInterface
{
    use HasUuid,
        EloquentDecoratorTrait;

    protected $casts = [
        'secret' => 'bool'
    ];

    public function __construct(array $attributes = [])
    {
        $this->setTable('achievements');

        $this->setHidden([
            'created_at',
            'updated_at'
        ]);

        $this->fillable([
            'name',
            'description',
            'icon',
            'points',
            'secret',
            'type',
        ]);

        parent::__construct($attributes);
    }

    public function setPoints(): int
    {
        return 1;
    }

    public function setSecret(): bool
    {
        return false;
    }

    public function isUnlocked(): bool
    {
        return $this->pivot->unlocked_at !== null;
    }

    public function isLocked(): bool
    {
        return !$this->isUnlocked();
    }

    public function triggerUnlocked(): void
    {
        event(new Unlocked($this));
        $this->whenUnlocked();
    }

    public function triggerProgress(): void
    {
        event(new Progress($this));
        $this->whenProgress();
    }

    protected function whenUnlocked(): void
    {

    }

    protected function whenProgress(): void
    {

    }

}
