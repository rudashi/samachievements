<?php

namespace Totem\SamAchievements\App\Model;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Totem\SamCore\App\Traits\HasUuid;

/**
 * @property string uuid
 * @property int user_id
 * @property int points
 * @property \Carbon\Carbon unlocked_at
 */
class AchievementProgress extends Pivot
{
    use HasUuid;

    public function __construct(array $attributes = [])
    {
        $this->setTable('achievement_progress');

        $this->setIncrementing(true);

        $this->setHidden([
            'achievement_id',
            'updated_at',
        ]);

        parent::__construct($attributes);
    }

}
