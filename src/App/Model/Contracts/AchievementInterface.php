<?php

namespace Totem\SamAchievements\App\Model\Contracts;

use Totem\SamAchievements\App\Model\AchievementProgress;

/**
 * @property string $id
 * @property string $name
 * @property string $description
 * @property string|null $icon
 * @property int $points
 * @property bool $secret
 * @property AchievementProgress pivot
 */
interface AchievementInterface
{

    public function isUnlocked(): bool;

    public function isLocked(): bool;

    public function triggerUnlocked(): void;

    public function triggerProgress(): void;

}