<?php

namespace Totem\SamAchievements\App\Model\Contracts;

interface Achievable
{

    public function setName(): string;

    public function setDescription(): string;

    public function setIcon(): ?string;

}