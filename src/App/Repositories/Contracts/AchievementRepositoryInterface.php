<?php

namespace Totem\SamAchievements\App\Repositories\Contracts;

use Illuminate\Support\Collection;
use Totem\SamAchievements\App\Model\Contracts\AchievementInterface;
use Totem\SamCore\App\Repositories\Contracts\RepositoryInterface;

interface AchievementRepositoryInterface extends RepositoryInterface
{

    public function allSimple(): Collection;

    public function registerAchievement(string $class = null): AchievementInterface;

    public function attachAchievement($user, $achievement, int $points = null): AchievementInterface;

}
