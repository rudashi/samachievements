<?php

namespace Totem\SamAchievements\App\Repositories;

use Totem\SamAchievements\App\Model\Achievement;
use Totem\SamAchievements\App\Model\Contracts\Achievable;
use Totem\SamAchievements\App\Traits\AchieverInterface;
use Totem\SamCore\App\Exceptions\RepositoryException;
use Totem\SamAchievements\App\Model\Contracts\AchievementInterface;
use Totem\SamCore\App\Repositories\BaseRepository;
use Totem\SamAchievements\App\Repositories\Contracts\AchievementRepositoryInterface;

class AchievementRepository extends BaseRepository implements AchievementRepositoryInterface
{

    public function model(): string
    {
       return Achievement::class;
    }

    public function allSimple(): \Illuminate\Support\Collection
    {
        return $this->all()->mapWithKeys(function($item) {
            return [class_basename($item->type) => $item->id];
        });
    }

    public function findBy(string $attribute, string $value, array $columns = ['*']): Achievement
    {
        $data = parent::findBy($attribute, $value, $columns);

        if ($data === null) {
            throw new RepositoryException( __('Given id :code is invalid or achievement not exist.', ['code' => $value]), 404);
        }
        return $data;
    }

    public function registerAchievement(string $class = null): AchievementInterface
    {
        if ($class === null) {
            throw new RepositoryException( __('No achievement class name has been given.') );
        }

        try {
            $className = "\\Totem\\SamAchievements\\App\\Achievements\\$class";
            /** @var Achievement|Achievable $model */
            $model = app()->make($className);

            if (parent::findBy('type', get_class($model))) {
                throw new RepositoryException( __('Achievement already registered.') );
            }

            $model->fill([
                'name' => $model->setName(),
                'description' => $model->setDescription(),
                'icon' => $model->setIcon(),
                'points' => $model->setPoints(),
                'secret' => $model->setSecret(),
                'type' => get_class($model),
            ]);
            $model->save();

            return $model;

        } catch (\Throwable $exception) {
            if ($exception instanceof RepositoryException) {
                throw new RepositoryException( $exception->getMessage()) ;
            }
            throw new \RuntimeException( $exception->getMessage()) ;
        }
    }

    public function attachAchievement($user, $achievement, int $points = null): AchievementInterface
    {
        $achievement = $achievement instanceof Achievable ? $achievement : $this->findBy('id',$achievement ?: 0);

        return $this->setProgressToAchiever(
            $user,
            $achievement,
            $points ?? $achievement->points
        );
    }

    private function getOrCreateProgressForAchiever(AchievementInterface $achievement, AchieverInterface $achiever): AchievementInterface
    {
        $progress = $achiever->achievements()->find($achievement);

        if ($progress === null) {
            $achiever->achievements()->attach($achievement);
        }
        return $achiever->achievements->find($achievement);

    }

    private function addPoints(AchievementInterface $progress, int $points): AchievementInterface
    {
        $recently_unlocked = false;

        if ($points > 0) {
            $progress->pivot->points += $points;
        } else {
            $progress->pivot->points = $points;
        }

        if ($progress->pivot->points >= $progress->points) {
            $recently_unlocked = true;
            $progress->pivot->points = $progress->points;
            $progress->pivot->unlocked_at = \Carbon\Carbon::now();
        }
        $progress->pivot->save();

        if ($recently_unlocked) {
            $progress->triggerUnlocked();
        } else {
            $progress->triggerProgress();
        }

        return $progress;
    }

    private function setProgressToAchiever(AchieverInterface $achiever, AchievementInterface $achievement, int $points): AchievementInterface
    {
        $progress = $this->getOrCreateProgressForAchiever($achievement, $achiever);

        if ($progress->isLocked()) {
            $progress = $this->addPoints($progress, $points);
        }

        return $progress;
    }

}
