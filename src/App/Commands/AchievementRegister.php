<?php

namespace Totem\SamAchievements\App\Commands;

use Totem\SamAchievements\App\Repositories\Contracts\AchievementRepositoryInterface;
use Illuminate\Console\Command;

class AchievementRegister extends Command
{

    protected $signature  = 'register:achievement {name : Class name of an achievement}';

    protected $description = 'Register a achievement in database';

    private AchievementRepositoryInterface $repository;

    public function __construct(AchievementRepositoryInterface $repository)
    {
        parent::__construct();

        $this->repository = $repository;
    }

    public function handle() : void
    {
        try {
            $model = $this->repository->registerAchievement($this->argument('name'));

            $this->line(\Carbon\Carbon::now() .' - Registered achievement: <fg=green>'. $model->name);
            $this->line('');

        } catch (\Totem\SamCore\App\Exceptions\RepositoryException $e) {
            $this->error($e->getMessage());
        } catch (\RuntimeException $e) {
            $this->error('Achievement class not exists.');
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

}
