<?php

namespace Totem\SamAchievements\App\Achievements;

use Totem\SamAchievements\App\Model\Achievement;
use Totem\SamAchievements\App\Model\Contracts\Achievable;

class UserReportedProblemInMessenger extends Achievement implements Achievable
{

    public function setName(): string
    {
        return 'No problem was reported';
    }

    public function setDescription(): string
    {
        return '';
    }

    public function setIcon(): ?string
    {
        return 'bug_report';
    }

    public function setSecret(): bool
    {
        return true;
    }

}