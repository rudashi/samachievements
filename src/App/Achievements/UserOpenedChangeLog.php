<?php

namespace Totem\SamAchievements\App\Achievements;

use Totem\SamAchievements\App\Model\Achievement;
use Totem\SamAchievements\App\Model\Contracts\Achievable;

class UserOpenedChangeLog extends Achievement implements Achievable
{

    public function setName(): string
    {
        return 'Scribe';
    }

    public function setDescription(): string
    {
        return 'Open changelog dialog';
    }

    public function setIcon(): ?string
    {
        return 'history_edu';
    }

}