<?php

namespace Totem\SamAchievements\App\Achievements;

use Totem\SamAchievements\App\Model\Achievement;
use Totem\SamAchievements\App\Model\Contracts\Achievable;

class UserOpenedConsole extends Achievement implements Achievable
{

    public function setName(): string
    {
        return 'Cake is a Lie';
    }

    public function setDescription(): string
    {
        return 'You found a terrible truth...';
    }

    public function setIcon(): ?string
    {
        return 'cake';
    }

}