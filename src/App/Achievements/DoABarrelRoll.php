<?php

namespace Totem\SamAchievements\App\Achievements;

use Totem\SamAchievements\App\Model\Achievement;
use Totem\SamAchievements\App\Model\Contracts\Achievable;

class DoABarrelRoll extends Achievement implements Achievable
{

    public function setName(): string
    {
        return 'Do a Barrel Roll!';
    }

    public function setDescription(): string
    {
        return '';
    }

    public function setIcon(): ?string
    {
        return 'autorenew';
    }

    public function setSecret(): bool
    {
        return true;
    }

}