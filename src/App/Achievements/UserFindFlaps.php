<?php

namespace Totem\SamAchievements\App\Achievements;

use Totem\SamAchievements\App\Model\Achievement;
use Totem\SamAchievements\App\Model\Contracts\Achievable;

class UserFindFlaps extends Achievement implements Achievable
{

    public function setName(): string
    {
        return 'Flappy Book';
    }

    public function setDescription(): string
    {
        return 'You have found the secret flaps!';
    }

    public function setIcon(): ?string
    {
        return 'mdi-twitter';
    }

    public function setSecret(): bool
    {
        return true;
    }

}