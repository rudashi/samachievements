<?php

namespace Totem\SamAchievements\App\Resources;

use Totem\SamCore\App\Resources\ApiCollection;

class AchievementCollection extends ApiCollection
{

    public $collects = AchievementResource::class;

}
