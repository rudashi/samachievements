<?php

namespace Totem\SamAchievements\App\Resources;

use Totem\SamCore\App\Resources\ApiResource;

/**
 * @property \Totem\SamAchievements\App\Model\Achievement $resource
 */
class AchievementResource extends ApiResource
{

    public function toArray($request): array
    {
        return [
            'id'                => $this->resource->id,
            'name'              => $this->resource->name,
            'icon'              => $this->resource->icon,
            'description'       => $this->when((bool) $this->resource->pivot, function() {
                return $this->resource->secret === false || $this->resource->pivot->unlocked_at ? $this->resource->description : null;
            }, $this->resource->secret === false ? $this->resource->description : null),
            'progress'          => $this->when((bool) $this->resource->pivot, function() {
                return $this->resource->pivot->points;
            }),
            'points'            => $this->resource->points,
            'unlocked_at'       => $this->when((bool) $this->resource->pivot, function() {
                if ($this->resource->pivot->unlocked_at instanceof \Carbon\Carbon) {
                    return $this->resource->pivot->unlocked_at->format('Y-m-d H:i:s');
                }
                return $this->resource->pivot->unlocked_at;
            }),
        ];
    }

}
