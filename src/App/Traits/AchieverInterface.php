<?php

namespace Totem\SamAchievements\App\Traits;

/**
 * @property \Illuminate\Database\Eloquent\Collection achievements
 */
interface AchieverInterface
{

    public function achievements(): \Illuminate\Database\Eloquent\Relations\BelongsToMany;

}
