<?php

namespace Totem\SamAchievements\App\Traits;

use Totem\SamAchievements\App\Model\Achievement;
use Totem\SamAchievements\App\Model\AchievementProgress;

trait Achiever
{

    public function achievements(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Achievement::class, 'achievement_progress')
            ->withTimestamps()
            ->using(AchievementProgress::class)
            ->withPivot([
                'points',
                'unlocked_at',
            ])
            ->orderBy('updated_at', 'desc');
    }

}
