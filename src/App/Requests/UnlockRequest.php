<?php

namespace Totem\SamAchievements\App\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UnlockRequest extends FormRequest
{

    public function rules(): array
    {
        return [
            'uuid' => 'required|uuid',
            'user_id' => 'integer|nullable',
        ];
    }

}
