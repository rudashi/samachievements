<?php

namespace Totem\SamAchievements\App\Events;

use Illuminate\Queue\SerializesModels;
use Totem\SamAchievements\App\Model\Contracts\AchievementInterface;

class Progress
{
    use SerializesModels;

    public AchievementInterface $achievement;

    public function __construct(AchievementInterface $achievement)
    {
        $this->achievement = $achievement;
    }

}
