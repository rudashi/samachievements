<?php

namespace Totem\SamAchievements\App\Controllers;

use Totem\SamCore\App\Resources\ApiResource;
use Totem\SamAchievements\App\Requests\UnlockRequest;
use Totem\SamAchievements\App\Resources\AchievementResource;
use Totem\SamAchievements\App\Resources\AchievementCollection;
use Totem\SamUsers\App\Repositories\Contracts\UserRepositoryInterface;
use Totem\SamAchievements\App\Repositories\Contracts\AchievementRepositoryInterface;
use Totem\SamCore\App\Controllers\ApiController;

class AchievementController extends ApiController
{

    private UserRepositoryInterface $userRepository;

    public function __construct(AchievementRepositoryInterface $repository, UserRepositoryInterface $userRepository)
    {
        $this->repository = $repository;
        $this->userRepository = $userRepository;
    }

    public function index(): AchievementCollection
    {
        return new AchievementCollection(
            $this->getFromRequestQuery($this->repository->all())
        );
    }

    public function trophy(): ApiResource
    {
        return new ApiResource($this->repository->allSimple());
    }

    public function userAchievement(int $id): AchievementCollection
    {
        return new AchievementCollection(
            $this->getFromRequestQuery($this->userRepository->findWithRelationsById($id, ['achievements'])->achievements)
        );
    }

    public function unlock(UnlockRequest $request, string $uuid, int $id)
    {
        return $this->progress($request, $uuid, $id, 1);
    }

    public function reset(UnlockRequest $request, string $uuid, int $id)
    {
        return $this->progress($request, $uuid, $id, 0);
    }

    public function set(UnlockRequest $request, string $uuid, int $id)
    {
        return $this->progress($request, $uuid, $id);
    }

    private function progress(UnlockRequest $request, string $uuid, int $id, int $points = null)
    {
        if ($request->input('uuid') !== $uuid || (int) $request->input('user_id', auth()->id()) !== $id) {
            return $this->response($this->error(422, __('Incorrect achievement parameters.')));
        }

        return new AchievementResource($this->repository->attachAchievement(
            $this->userRepository->findById($id),
            $this->repository->findBy('id', $uuid),
            $points
        ));
    }

}
