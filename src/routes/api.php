<?php

use Illuminate\Support\Facades\Route;
use Totem\SamAchievements\App\Controllers\AchievementController;

Route::group(['prefix' => 'api' ], static function() {

    Route::middleware(config('sam-admin.guard-api'))->group(static function() {

        Route::get('trophy', [AchievementController::class, 'trophy']);

        Route::group(['prefix' => 'achievements'], static function() {
            Route::get('/', [AchievementController::class, 'index']);
            Route::post('{uuid}/unlock/{id}', [AchievementController::class, 'unlock']);
            Route::post('{uuid}/reset/{id}', [AchievementController::class, 'reset']);
            Route::post('{uuid}/set/{id}', [AchievementController::class, 'set']);
        });

        Route::group(['prefix' => 'users'], static function() {
            Route::get('{id}/achievements', [AchievementController::class, 'userAchievement']);
        });

    });

});
