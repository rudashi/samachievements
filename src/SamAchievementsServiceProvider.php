<?php

namespace Totem\SamAchievements;

use Totem\SamCore\App\Traits\TraitServiceProvider;
use Illuminate\Support\ServiceProvider;

class SamAchievementsServiceProvider extends ServiceProvider
{
    use TraitServiceProvider;

    public function getNamespace(): string
    {
        return 'sam-achievement';
    }

    public function boot(): void
    {
        $this->loadAndPublish(
            __DIR__ . '/resources/lang',
            __DIR__ . '/database/migrations'
        );

        $this->loadRoutesFrom(__DIR__.'/routes/api.php');
    }

    public function register(): void
    {
        $this->registerEloquentFactoriesFrom(__DIR__. '/database/factories');

        $this->commands([
            \Totem\SamAchievements\App\Commands\AchievementRegister::class
        ]);

        $this->configureBinding([
            \Totem\SamAchievements\App\Repositories\Contracts\AchievementRepositoryInterface::class => \Totem\SamAchievements\App\Repositories\AchievementRepository::class,
            \Totem\SamUsers\App\Repositories\Contracts\UserRepositoryInterface::class =>  \Totem\SamUsersBundle\App\Repositories\UserRepository::class,
        ]);
    }

}
