<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAchievementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function up(): void
    {
        try{
            Schema::create('achievements', static function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->timestamps();
                $table->string('name');
                $table->text('description')->nullable();
                $table->string('icon')->nullable();
                $table->unsignedInteger('points')->default(1);
                $table->tinyInteger('secret')->default(0);
                $table->string('type');
            });

            Schema::create('achievement_progress', static function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->timestamps();
                $table->uuid('achievement_id');
                $table->integer('user_id')->unsigned();
                $table->unsignedInteger('points')->default(0);
                $table->timestamp('unlocked_at')->nullable()->default(null);

                $table->foreign('user_id')->references('id')->on('users')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('achievement_id')->references('id')->on('achievements')
                    ->onUpdate('cascade')->onDelete('cascade');
            });

        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down(): void
    {
        Schema::dropIfExists('achievement_progress');
        Schema::dropIfExists('achievements');
    }

}
