<?php

use Faker\Generator as Faker;

/**
 * @var $factory \Illuminate\Database\Eloquent\Factory
 */
$factory->define(\Tests\FakeAchievement::class, static function (Faker $faker) {
    return [
        'name' => 'Fake Achievement',
        'description' => 'Fake Description',
        'icon' => null,
        'points' => 1,
        'secret' => 0,
        'type' => \Tests\FakeAchievement::class,
    ];
});
